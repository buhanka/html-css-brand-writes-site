<?php require './header.php'; ?>

<div class="meet-team-page marhead">

  <section class="page-title marbot">
    <div class="container">
      <h1>Meet the Team</h1>
      <p>
        Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna volutpat.
      </p>
    </div>
  </section>

  <section class="container page-container marbot">
    <div class="row maroff">
      <div class="col-lg-9 main-content padoff">
        <div class="page-content marbot">
          <div class="row">
            <div class="col-lg-4">
              <div class="team-background">
                <img src="./img/team/team1.png" alt="" class="team-image">
              </div>
              <h2 class="team-name">Nick Aries</h2>
              <p class="team-position">UK Partner</p>
            </div>
            <div class="col-lg-4">
              <div class="team-background">
                <img src="" alt="" class="team-image">
              </div>
              <h2 class="team-name">Robert Milligan</h2>
              <p class="team-position">Associate</p>
            </div>
            <div class="col-lg-4">
              <div class="team-background">
                <img src="./img/team/team3.png" alt="" class="team-image">
              </div>
              <h2 class="team-name">Lorraine Tay</h2>
              <p class="team-position">Joint Managing Partner</p>
            </div>
            <div class="col-lg-4">
              <div class="team-background">
                <img src="./img/team/team3.png" alt="" class="team-image">
              </div>
              <h2 class="team-name">Lorraine Tay</h2>
              <p class="team-position">Joint Managing Partner</p>
            </div>
            <div class="col-lg-4">
              <div class="team-background">
                <img src="./img/team/team1.png" alt="" class="team-image">
              </div>
              <h2 class="team-name">Robert Milligan</h2>
              <p class="team-position">Associate</p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 sidebar">
        <div class="sidebar-content">
            <h1>Explore</h1>
            <ul class="sidebar-menu dark">
              <li><a href="#">About Us</a></li>
              <li><a href="#" class="active">Meet the Team</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Terms & Conditions</a></li>
            </ul>
        </div>
      </div>
    </div>
  </section>


</div>

<?php require './footer.php'; ?>
