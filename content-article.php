<?php require './header.php'; ?>

<div class="article-page top-space">
  <div class="overlay-dark">
  </div>

  <section class="container marbot">
    <div class="post-thumbnail">
      <div class="thumbnail-overlay video">
        <h2><a href="">How the lightbulb became the first trade mark in history</a></h2>
        <div class="btn-line">
          <a class="btn-bord">Frederik Parker</a>
          <a class="btn-bord btn-tag">Industry</a>
        </div>
        <a href="#" class="video-overlay">PLAY VIDEO</a>
      </div>
      <img src="./img/article/thumbnail.jpg" alt="">
    </div>
  </section>

  <section class="container post-container marbot">
    <div class="row maroff">
      <div class="col-lg-9 main-content padoff">
        <div class="post-content marbot">
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
            </p>
            <h1>Heading Example</h1>
            <p>
              Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
            </p>
            <p>
              Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
            </p>

            <div class="social-media">
              <a href="" class="social-linkedin"></a>
              <a href="" class="social-twitter"></a>
              <a href="" class="social-printerest"></a>
              <a href="" class="social-google"></a>
              <a href="" class="social-email"></a>
            </div>
        </div>

        <div class="row author maroff">
            <div class="col-lg-3 padoff author-img">
              <img src="./img/article/author-img.jpg" alt="">
            </div>
            <div class="col-lg-9 author-desc">
                <h2>Lorraine Anne Tay</h2>
                <p>Lorraine is highly regarded as a leading intellectual property practitioner with experience in international and cross-border issues.</p>
                <a href="#">Browse more from Lorraine</a>
            </div>
        </div>

        <div class="comment-title marbot">
          <h1>Have your say</h1>
        </div>
        <div class="comment-field">
          <textarea name="name" id="comment-area" rows="6" cols="94"></textarea>
          <button id="btn-send-comment">Post comment</button>
        </div>

        <div class="comments">
            <div class="single-comment">
              <p class="comment-body">Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
              <p class="comment-author">User name here</p>
            </div>
            <div class="single-comment">
              <p class="comment-body">Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
              <p class="comment-author">User name here</p>
            </div>
        </div>

        <div class="more-comments" align="center">
          <a href="#" class="load-comments">Load more comments</a>
        </div>

      </div>
      <div class="col-lg-3 sidebar">
        <div class="sidebar-content">
            <h1>Popular</h1>
            <div class="row maroff">
                <div class="col-lg-12 padoff">
                  <div class="popular-rate">
                    1
                  </div>
                  <div class="popular">
                    <a href="">
                      <img src="./img/home/tranding-img1.png" class="popular-img">
                    </a>
                  </div>
                  <p>
                    Increase in workplace engagement and appreciation study shows
                  </p>
                </div>
                <div class="col-lg-12 padoff">
                  <div class="popular-rate">
                    2
                  </div>
                  <div class="popular">
                    <a href="">
                      <img src="./img/home/latest-img1.jpg" class="popular-img">
                    </a>
                  </div>
                  <p>
                    Increase in workplace engagement and appreciation study shows
                  </p>
                </div>
                <div class="col-lg-12 padoff">
                  <div class="popular-rate">
                    3
                  </div>
                  <div class="popular">
                    <a href="">
                      <img src="./img/home/latest-img2.jpg" class="popular-img">
                    </a>
                  </div>
                  <p>
                    Increase in workplace engagement and appreciation study shows
                  </p>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>

  <div class="container dark marbot">
    <h1>Related Stories</h1>
  </div>

  <div class="container-fluid related-story-container">
    <section class="container">
        <div class="row related-story-list">
          <div class="col-lg-4">
              <div class="card">
                <a href="">
                  <img src="./img/home/latest-img1.jpg" class="card-img">
                </a>
                <div class="card-container">
                  <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
                  <p><a href="">Frederik Parker</a></p>
                </div>
             </div>
          </div>
          <div class="col-lg-4">
              <div class="card">
                <a href="">
                  <img src="./img/home/latest-img2.jpg" class="card-img">
                </a>
                <div class="card-container">
                  <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
                  <p><a href="">Frederik Parker</a></p>
                </div>
             </div>
           </div>
          <div class="col-lg-4">
              <div class="card">
                <a href="">
                  <img src="./img/home/latest-img3.jpg" class="card-img">
                </a>
              <div class="card-container">
                <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
                <p><a href="">Frederik Parker</a></p>
              </div>
           </div>
         </div>
        </div>

        <a href="#" class="underline dark">Browse more articles in Industry</a>

    </section>
  </div>

</div>

<?php require './footer.php'; ?>
