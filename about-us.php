<?php require './header.php'; ?>

<div class="about-us-page marhead">

  <section class="page-title marbot">
    <div class="container">
      <h1>About Us</h1>
      <p>
        Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna volutpat.
      </p>
    </div>
  </section>

  <section class="container page-container marbot">
    <div class="row maroff">
      <div class="col-lg-9 main-content padoff">
        <div class="page-content marbot">
            <p>
              At Bird & Bird we're passionate about brands. BrandWrites by Bird & Bird is an international publication that explores topical legal and industry related brand news, featuring recent trade mark cases and key changes to the law, practical advice and commentary from respected brand owners. It features contributions from Bird & Bird's renowned IP team across Europe, Asia-Pacific and the Middle East.
            </p>
            <p>
              Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
            </p>
            <h1>Subtitle Heading</h1>
            <p>
              Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
            </p>
            <p>
              Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu.
            </p>

        </div>
      </div>
      <div class="col-lg-3 sidebar">
        <div class="sidebar-content">
            <h1><a href="#subscribe" class="modal-link dark" data-toggle="modal">Explore</a></h1>
            <ul class="sidebar-menu dark">
              <li><a href="#">About Us</a></li>
              <li><a href="#">Meet the Team</a></li>
              <li><a href="#">Contact Us</a></li>
              <li><a href="#">Terms & Conditions</a></li>
            </ul>
        </div>
      </div>
    </div>
  </section>

  <div id="subscribe" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#9747;</button>
        </div>
        <div class="modal-body">
          <p>Subscribe now to receive updates to your inbox</p>
          <input type="text" name="name" placeholder="Enter your full name">
          <input type="text" name="email" placeholder="Enter your email">
          <button type="button" id="btn-subscribe">Subscribe</button>
          <a href="#policy-modal" id="policy" data-toggle="modal">Read our privacy policy here</a>
        </div>
      </div>
    </div>
  </div>

  <div id="policy-modal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#9747;</button>
          <h4 class="modal-title">Privacy Policy</h4>
        </div>
        <div class="modal-body">
          <p>
            At Bird & Bird we're passionate about brands. BrandWrites by Bird & Bird is an international publication that explores topical legal and industry related brand news, featuring recent trade mark cases and key changes to the law, practical advice and commentary from respected brand owners. It features contributions from Bird & Bird's renowned IP team across Europe, Asia-Pacific and the Middle East.
          </p>
          <p>
            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
          </p>
          <p>
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
          </p>
          <p>
            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.
          </p>
          <p>
            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu.
          </p>
        </div>
      </div>
    </div>
  </div>

</div>

<?php require './footer.php'; ?>
