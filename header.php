<!DOCTYPE html>
<html>
  <head>
    <meta charset="ASCII">
    <title>Brand Writes</title>
    <link rel="stylesheet" href="./css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/slick-theme.css"/>
    <link rel="stylesheet" href="./css/slick.css"/>
    <link rel="stylesheet" href="./css/main.css">
    <link rel="stylesheet" href="./css/font-awesome.min.css">
    <!-- Fonts -->
  </head>
<body>
  <div class="body-overlay"></div>
  <nav class="navbar navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="title">
          <a href="/" class="navbar-brand logo"><b>Brand</b>Writes</a>
          <a href="/" class="desc">by the Intellectual Property Group of Bird&Bird</a>
        </div>
        <div class="icon">
          <a href="#"><img src="./img/home//home-icon.svg" class="home-icon" alt=""></a>
        </div>
      </div>
      <div id="navbar" class="parent-nav collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li>
            <a class="current-page" href="/listing.php">INDUSTRY</a>
            <ul class="dropdown-content row maroff">
                <li class="col-lg-push-2">
                  <ul>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                  </ul>
                </li>
                <li class="col-lg-push-4">
                  <ul>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                  </ul>
                </li>
                <li class="col-lg-push-6">
                  <ul>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                  </ul>
                </li>
            </ul>
          </li>
          <li><a href="/content-article.php">CASE REPORTS</a></li>
          <li><a href="/about-us.php">INTERVIEWS</a></li>
          <li><a href="/meet-team.php">FEATURES</a>
            <ul class="dropdown-content row maroff">
                <li class="col-lg-push-2">
                  <ul>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                  </ul>
                </li>
                <li class="col-lg-push-4">
                  <ul>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                  </ul>
                </li>
                <li class="col-lg-push-6">
                  <ul>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                    <li>Sub-Category name</li>
                  </ul>
                </li>
            </ul>
          </li>
          <li><a href="#contact">JURISDICTIONS</a></li>
        </ul>
        <form class="navbar-form navbar-right" role="search">
            <div class="inner-addon left-addon">
              <i class="glyphicon glyphicon-search"></i>
              <input type="text" class="form-control search-input" id="seacrh" placeholder="Search" />
            </div>
        </form>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
  <div class="collapse" id="search-bar" align="center">
    <form role="search" class="search-bar-form">
          <input autofocus type="text" placeholder="Start typing here" id="search-bar-input">
    </form>

  </div>
  <div id="overlay-search"></div>
