<?php require("./header.php") ?>

<div class="list-page">

  <section class="top-image marbot">
    <div class="top-image-overlay">
      <div class="container">
        <div class="top-image-text">
          <h1>Industry</h1>
          <p>
            Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, ipsum dolor sit amet, consectetuer adipiscing elit sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.
          </p>
        </div>
        <a class="btn-bord">Sort by: Latest</a>
      </div>
    </div>
  </section>

  <section class="container list">
    <div class="row">
      <div class="col-lg-4">
          <div class="card">
            <div class="overlay-file">
            </div>
            <div class="card-img">
              <a href="">
                <img src="./img/home/brand-img1.jpg" class="">
              </a>
            </div>
            <div class="card-container">
              <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
              <p><a href="">Frederik Parker</a></p>
            </div>
         </div>
      </div>
      <div class="col-lg-4">
          <div class="card">
            <div class="overlay-media">
            </div>
            <div class="card-img">
              <a href="">
                <img src="./img/home/brand-img2.jpg" class="">
              </a>
            </div>
            <div class="card-container">
              <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
              <p><a href="">Frederik Parker</a></p>
            </div>
         </div>
       </div>
      <div class="col-lg-4">
          <div class="card">
            <div class="card-img">
              <a href="">
                <img src="./img/home/brand-img3.jpg" class="">
              </a>
            </div>
          <div class="card-container">
            <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
            <p><a href="">Frederik Parker</a></p>
          </div>
       </div>
     </div>
     <div class="col-lg-4">
         <div class="card">
           <div class="overlay-file">
           </div>
           <div class="card-img">
             <a href="">
               <img src="./img/home/brand-img1.jpg" class="">
             </a>
           </div>
           <div class="card-container">
             <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
             <p><a href="">Frederik Parker</a></p>
           </div>
        </div>
     </div>
     <div class="col-lg-4">
         <div class="card">
           <div class="overlay-media">
           </div>
           <div class="card-img">
             <a href="">
               <img src="./img/home/brand-img2.jpg" class="">
             </a>
           </div>
           <div class="card-container">
             <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
             <p><a href="">Frederik Parker</a></p>
           </div>
        </div>
      </div>
     <div class="col-lg-4">
         <div class="card">
           <div class="card-img">
             <a href="">
               <img src="./img/home/brand-img3.jpg" class="">
             </a>
           </div>
         <div class="card-container">
           <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
           <p><a href="">Frederik Parker</a></p>
         </div>
      </div>
    </div>
    </div>

    <div class="pagination">
      <a href="#">
        <div class="pagination-page active-page">
        1
        </div>
      </a>
      <a href="#">
        <div class="pagination-page">
        2
        </div>
      </a>
      <a href="#">
        <div class="pagination-page">
        3
        </div>
      </a>
      <a href="#">
        <div class="pagination-page">
        4
        </div>
      </a>
      <a href="#">
        <div class="pagination-page">
        5
        </div>
      </a>
    </div>
  </section>

</div>

<?php require("./footer.php") ?>
