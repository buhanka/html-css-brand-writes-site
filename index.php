<?php require './header.php'; ?>

<div class="main-page top-space">
  <section class="container tranding-news marbot">
    <div class="row valign head">
      <div class="col-lg-2 off-1" align="center">
        <span>Trending News:</span>
      </div>
      <div class="col-lg-10 off-2">
          <div class="col-lg-11">
            <div class="tranding-slider">
              <a href="#">This is a news headline for an article 1</a>
              <a href="#">This is a news 2</a>
              <a href="#">This is a news headline for an article and headline for an article 3</a>
            </div>
          </div>
          <div class="col-lg-1 text-right">
            <button class="arrow-left"></button>
            <button class="arrow-right"></button>
          </div>
        </div>
    </div>


<a href="#">
  <div class="">

  </div>
</a>
    <div class="container">
      <div class="row posts">
          <div class="col-lg-6 two-grid">
            <div class="row">
              <div class="col-lg-12 first">
                <div class="tranding-overlay">
                  <h2><a href="">Increase in workplace engagement and appreciation study shows</a></h2>
                  <a class="btn-bord">Frederik Parker</a>
                </div>
              </div>
              <div class="col-lg-12 second">
                <div class="tranding-overlay ">
                  <h2><a href="">One in ten lawyers know the truth about their boss</a></h2>
                  <a class="btn-bord">Frederik Parker</a>
                </div>
              </div>
            </div>
        </div>
        <div class="col-lg-6 last">
          <div class="tranding-overlay-down">
            <h2><a href="">Dubai is set to become the new super power says sultan</a></h2>
            <a class="btn-bord">Frederik Parker</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="container latest-story marbot">
      <h1 class="marbot">Latest story</h1>
      <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-img">
                  <a href="">
                    <img src="./img/home/latest-img1.jpg" class="">
                  </a>
                </div>
              <div class="card-container">
                <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
                <p><a href="">Frederik Parker</a></p>
              </div>
           </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
              <a href="">
                <div class="card-img">
                  <img src="./img/home/latest-img2.jpg" class="">
                </div>
              </a>
              <div class="card-container">
                <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
                <p><a href="">Frederik Parker</a></p>
              </div>
           </div>
         </div>
        <div class="col-lg-4">
            <div class="card">
              <a href="">
                <div class="card-img">
                  <img src="./img/home/latest-img3.jpg" class="">
                </div>
              </a>
            <div class="card-container">
              <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
              <p><a href="">Frederik Parker</a></p>
            </div>
         </div>
       </div>
      </div>
  </section>

  <section class="container branding-news marbot">
    <h1>More Branding News</h1>
    <div class="row">
      <div class="col-lg-4">
          <div class="card">
            <div class="overlay-file">
            </div>
            <div class="card-img">
              <a href="">
                <img src="./img/home/brand-img1.jpg" class="">
              </a>
            </div>
            <div class="card-container">
              <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
              <p><a href="">Frederik Parker</a></p>
            </div>
         </div>
      </div>
      <div class="col-lg-4">
          <div class="card">
            <div class="overlay-media">
            </div>
            <div class="card-img">
              <a href="">
                <img src="./img/home/brand-img2.jpg" class="">
              </a>
            </div>
            <div class="card-container">
              <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
              <p><a href="">Frederik Parker</a></p>
            </div>
         </div>
       </div>
      <div class="col-lg-4">
          <div class="card">
            <div class="card-img">
              <a href="">
                <img src="./img/home/brand-img3.jpg" class="">
              </a>
            </div>
          <div class="card-container">
            <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
            <p><a href="">Frederik Parker</a></p>
          </div>
       </div>
     </div>
     <div class="col-lg-4">
         <div class="card">
           <div class="overlay-file">
           </div>
           <div class="card-img">
             <a href="">
               <img src="./img/home/brand-img1.jpg" class="">
             </a>
           </div>
           <div class="card-container">
             <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
             <p><a href="">Frederik Parker</a></p>
           </div>
        </div>
     </div>
     <div class="col-lg-4">
         <div class="card">
           <div class="overlay-media">
           </div>
           <div class="card-img">
             <a href="">
               <img src="./img/home/brand-img2.jpg" class="">
             </a>
           </div>
           <div class="card-container">
             <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
             <p><a href="">Frederik Parker</a></p>
           </div>
        </div>
      </div>
     <div class="col-lg-4">
         <div class="card">
           <div class="card-img">
             <a href="">
               <img src="./img/home/brand-img3.jpg" class="">
             </a>
           </div>
         <div class="card-container">
           <h2><a href="">How wearing a red tie is a symbol of masculinity</a></h2>
           <p><a href="">Frederik Parker</a></p>
         </div>
      </div>
    </div>
    </div>
  </section>

    <div class="container-fluid social">
      <div class="container">
        <a href="" class="twitter"></a>
        <div class="social-slider" align="center">
          <div class="slider-inner">
            <h1>We’ve just launched our brand new site. Take a look at brandwrites.law</h1>
            <p><a href="">Join us on Twitter</a></p>
          </div>
          <div class="slider-inner">
            <h1>We’ve just launched our brand new site. Take a look at brandwrites.law</h1>
            <p><a href="">Join us on Twitter</a></p>
          </div>
          <div class="slider-inner">
            <h1>We’ve just launched our brand new site. Take a look at brandwrites.law</h1>
            <p><a href="">Join us on Twitter</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php require './footer.php'; ?>
