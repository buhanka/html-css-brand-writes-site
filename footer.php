<footer class="container-fluid footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-lg-offset-1">
        <h3><a href="">BrandWrites</a></h3>
        <ul>
          <li><a href="">About Us</a></li>
          <li><a href="">Contact Us</a></li>
          <li><a href="">Terms & Conditions</a></li>
        </ul>
      </div>
      <div class="col-lg-2">
        <h3><a href="">Browse</a></h3>
        <ul>
          <li><a href="">Industry</a></li>
          <li><a href="">News</a></li>
          <li><a href="">Intellectual Property</a></li>
        </ul>
      </div>
      <div class="col-lg-2">
        <h3><a href="">Our Other Sites</a></h3>
        <ul>
          <li><a href="">MediaWrites</a></li>
          <li><a href="">BrandWrites</a></li>
          <li><a href="">DesignWrites</a></li>
        </ul>
      </div>
      <div class="col-lg-2 col-lg-offset-3 logo-footer">
        <a href="">
          <span>By the IP Group of</span>
          <img src="./img/home//logo-footer.png" alt="">
        </a>
      </div>
    </div>
  </div>
</footer>

<script src="./js/jquery.min.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/slick.js"></script>
<script src="./js/main.js"></script>
</body>
</html>
