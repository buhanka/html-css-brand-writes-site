//Search bar
jQuery("#seacrh").focus(function() {
  jQuery("#search-bar").animate({top : "60px"}, 600).addClass("in");
  jQuery("#overlay-search").css("display", "block")

});

jQuery("#overlay-search").click(function(){
  jQuery("#search-bar").animate({top : "-500px"}, 600);
  jQuery("#overlay-search").css("display", "none")
});

//tranding slider
jQuery(".tranding-slider").slick({
  arrows: true,
  prevArrow: jQuery(".arrow-left"),
  nextArrow: jQuery(".arrow-right"),
});

//slider for social media
jQuery(".social-slider").slick({
  dots: true,
  arrows: false,
});

//Video overlay
jQuery(".video-overlay").click(function(){
  jQuery(".overlay-dark").css("display", "block")
  jQuery(".post-thumbnail").addClass("on")
})
jQuery(".overlay-dark").click(function(){
  jQuery(".overlay-dark").css("display", "none")
  jQuery(".post-thumbnail").removeClass("on")
})
